###
# MySQL
#
# MySQL is a scalable relational database
###

resource "google_compute_global_address" "mysql" {
  count = var.mysql_enabled ? 1 : 0
  name = "mysql-private-ip-${var.workspace}"
  purpose = "VPC_PEERING"
  address_type = "INTERNAL"
  prefix_length = 16
  network = google_compute_network.container_network.self_link
}

resource "random_integer" "db_instance_name" {
  count = var.mysql_enabled ? 1 : 0
  max = 99999
  min = 10000
}

resource "google_service_networking_connection" "mysql" {
  count = var.mysql_enabled ? 1 : 0
  network = google_compute_network.container_network.self_link
  service = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [
    google_compute_global_address.mysql[count.index].name
  ]
}

resource "google_sql_database_instance" "master" {
  count = var.mysql_enabled ? 1 : 0
  database_version = var.mysql_version
  depends_on = [
    google_service_networking_connection.mysql
  ]

  # Names must be randomised as cannot be reused for 7 days after deletion
  name = "master-${var.workspace}-${random_integer.db_instance_name[count.index].result}"

  settings {
    tier = var.mysql_tier
    user_labels = {
      is_replica = "false"
      created_by = "terraform"
      env = var.workspace
    }

    ip_configuration {
      ipv4_enabled = var.mysql_enable_public_access
      private_network = google_compute_network.container_network.self_link
      require_ssl = var.mysql_require_ssl
    }

    maintenance_window {
      day = var.mysql_maintenance_window_day
      hour = var.mysql_maintenance_window_hour
      update_track = var.mysql_maintenance_window_track
    }

    backup_configuration {
      binary_log_enabled = var.mysql_backup_enabled
      enabled = var.mysql_backup_enabled
      start_time = var.mysql_backup_start_time
    }
  }
}

resource "google_sql_database" "default" {
  count = var.mysql_enabled ? 1 : 0
  depends_on = [
    google_sql_database_instance.master
  ]

  name = var.mysql_db_name
  instance = google_sql_database_instance.master[count.index].name
  charset = var.mysql_db_charset
  collation = var.mysql_db_collation
}

resource "random_string" "mysql_username" {
  length = 14
  special = false
  lower = true
  upper = false
  number = false
}

resource "random_password" "mysql_password" {
  length = 16
  special = true
}

resource "google_sql_user" "default" {
  count = var.mysql_enabled ? 1 : 0
  depends_on = [
    google_sql_database.default
  ]

  instance = google_sql_database_instance.master[count.index].name
  name = random_string.mysql_username.result
  password = random_password.mysql_password.result
}

resource "google_sql_database_instance" "failover_replica" {
  count = var.mysql_enabled ? var.mysql_failover_replica_count : 0

  # Names must be randomised as cannot be reused for 7 days after deletion
  name = "failover-${var.workspace}-${random_integer.db_instance_name[count.index].result}"

  database_version = var.mysql_version
  depends_on = [
    google_sql_database_instance.master,
    google_sql_database.default,
    google_sql_user.default,
  ]

  # The name of the instance that will act as the master in the replication setup.
  master_instance_name = google_sql_database_instance.master[count.index].name

  replica_configuration {
    # Specifies that the replica is the failover target.
    failover_target = true
  }

  settings {
    tier = var.mysql_tier
    user_labels = {
      is_replica = "true"
      type = "failover"
      created_by = "terraform"
      env = var.workspace
    }

    ip_configuration {
      ipv4_enabled = var.mysql_enable_public_access
      private_network = google_compute_network.container_network.self_link
      require_ssl = var.mysql_require_ssl
    }
  }
}

resource "google_sql_database_instance" "read_replica" {
  count = var.mysql_enabled ? var.mysql_read_replica_count : 0

  # Names must be randomised as cannot be reused for 7 days after deletion
  name = "read-${var.workspace}-${random_integer.db_instance_name[count.index].result}"

  database_version = var.mysql_version
  depends_on = [
    google_sql_database_instance.master,
    google_sql_database.default,
    google_sql_user.default,
    google_sql_database_instance.failover_replica
  ]

  # The name of the instance that will act as the master in the replication setup.
  master_instance_name = google_sql_database_instance.master[count.index].name

  replica_configuration {
    # Specifies that the replica is not the failover target.
    failover_target = false
  }

  settings {
    tier = var.mysql_tier

    user_labels = {
      is_replica = "true"
      replica_type = "read"
      created_by = "terraform"
      env = var.workspace
    }

    ip_configuration {
      ipv4_enabled = var.mysql_enable_public_access
      private_network = google_compute_network.container_network.self_link
      require_ssl = var.mysql_require_ssl
    }
  }
}
