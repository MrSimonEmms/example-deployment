###
# Networks
#
# The networks exist to allow the Kubernetes stack to
# communicate with private resources such as the MySQL
# database
###

resource "google_compute_network" "container_network" {
  name = "container-network-${var.workspace}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "container_subnetwork" {
  name = "container-subnetwork-${var.workspace}"
  ip_cidr_range = "10.2.0.0/16"
  region = var.location
  network = google_compute_network.container_network.self_link
}
