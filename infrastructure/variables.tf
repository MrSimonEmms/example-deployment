variable "gcp_credentials" {
  description = "Google Cloud Platform service account JSON credentials"
}

variable "workspace" {
  description = "Workspace name"
}

variable "location" {
  description = "Location where the resources are created"
  default = "europe-west2"
}

variable "k8s_enabled" {
  type = bool
  description = "Decides whether to enable the Kubernetes cluster creation"
  default = true
}

variable "k8s_auto_repair" {
  type = bool
  description = "Enable auto repair for the Kubernetes cluster"
  default = true
}

variable "k8s_auto_upgrade" {
  type = bool
  description = "Enable auto upgrade for the Kubernetes version on the cluster"
  default = false
}

variable "k8s_maintenance_start_time" {
  description = "The time in UTC to start the maintenance window"
  default = "01:00"
}

variable "k8s_machine_type" {
  description = "The machine type to use for the Kubernetes machines"
  default = "n1-standard-1"
}

variable "k8s_min_node_count" {
  type = number
  description = "The minimum number of machines for the Kubernetes cluster"
  default = 1
}

variable "k8s_max_node_count" {
  type = number
  description = "The maximum number of machines for the Kubernetes cluster"
  default = 3
}

variable "k8s_version_prefix" {
  description = "Only return versions that match the string prefix. Append a dot (.) to ensure versions correctly referenced"
  type = string
  default = null
}

variable "mysql_enabled" {
  type = bool
  description = "Decides whether to enable MySQL"
  default = true
}

variable "mysql_version" {
  description = "The MySQL version to use"
  default = "MYSQL_5_7"
}

variable "mysql_tier" {
  description = "The machine type to use for the database instances"
  default = "db-f1-micro"
}

variable "mysql_enable_public_access" {
  type = bool
  description = "Decides whether to allow public access to the MySQL instances"
  default = false
}

variable "mysql_maintenance_window_day" {
  type = number
  description = "The day to perform maintenance, 1-7 starting on Monday"
  default = 7
}

variable "mysql_maintenance_window_hour" {
  type = number
  description = "The UTC time to start maintenance"
  default = 1
}

variable "mysql_maintenance_window_track" {
  description = "The types of updates to receive - stable or canary"
  default = "stable"
}

variable "mysql_backup_enabled" {
  type = bool
  description = "Decides whether to enable MySQL backups"
  default = false
}

variable "mysql_backup_start_time" {
  description = "The time to start backups in UTC"
  default = "06:00"
}

variable "mysql_failover_replica_count" {
  type = number
  description = "The number of failover replicas to have"
  default = 0
}

variable "mysql_read_replica_count" {
  type = number
  description = "The number of read replicas to have"
  default = 0
}

variable "mysql_require_ssl" {
  type = bool
  description = "Forces connects to be made over SSL"
  default = false
}

variable "mysql_db_name" {
  description = "The database name"
  default = "db_name"
}

variable "mysql_db_charset" {
  type = string
  description = "The database charset"
  default = null
}

variable "mysql_db_collation" {
  type = string
  description = "The database collation"
  default = null
}
