provider "google" {
  version = "~> 3.18"
  credentials = var.gcp_credentials
  region = var.location
}

provider "random" {
  version = "~> 2.2"
}

provider "template" {
  version = "~> 2.1"
}

