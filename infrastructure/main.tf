terraform {
  backend "remote" {
    organization = "mrsimonemms"
    workspaces {
      prefix = "example-deployment-"
    }
  }
}
