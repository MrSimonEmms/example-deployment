output "kubeconfig" {
  value = try(data.template_file.kubeconfig.0.rendered, null)
  sensitive = true
}

output "mysql_user" {
  value = try(google_sql_user.default.0.name, null)
  sensitive = true
}

output "mysql_password" {
  value = try(google_sql_user.default.0.password, null)
  sensitive = true
}

output "mysql_db" {
  value = var.mysql_db_name
  sensitive = true
}

output "mysql_host" {
  value = try(google_sql_database_instance.master.0.private_ip_address, null)
}
