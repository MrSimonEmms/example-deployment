###
# Kubernetes
#
# Kubernetes is an open source orchestation tool that
# allows you to deploy Docker containers easily and
# repeatably to provide reliable production applications
###

resource "random_string" "k8s_username" {
  length = 14
  special = false
  lower = true
  upper = false
  number = false
}

resource "random_password" "k8s_password" {
  length = 18
}

# Get latest version available in the given zone
data "google_container_engine_versions" "current" {
  count = var.k8s_enabled ? 1 : 0
  location = var.location
  version_prefix = var.k8s_version_prefix
}

resource "google_container_cluster" "primary" {
  count = var.k8s_enabled ? 1 : 0
  name = "k8s-cluster-${var.workspace}"
  location = var.location
  min_master_version = data.google_container_engine_versions.current[count.index].latest_master_version

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1

  network = google_compute_network.container_network.name
  subnetwork = google_compute_subnetwork.container_subnetwork.name

  resource_labels = {
    created_by = "terraform"
    env = var.workspace
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block  = "10.0.0.0/16"
    services_ipv4_cidr_block = "10.1.0.0/16"
  }

  master_auth {
    username = random_string.k8s_username.result
    password = random_password.k8s_password.result

    client_certificate_config {
      issue_client_certificate = true
    }
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = var.k8s_maintenance_start_time
    }
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  count = var.k8s_enabled ? 1 : 0
  name = "k8s-pool-${var.workspace}"
  location = var.location
  cluster = google_container_cluster.primary[count.index].name
  node_count = var.k8s_min_node_count
  version = var.k8s_auto_upgrade == false ? data.google_container_engine_versions.current[count.index].latest_master_version : null

  autoscaling {
    max_node_count = var.k8s_max_node_count
    min_node_count = var.k8s_min_node_count
  }

  management {
    auto_repair = var.k8s_auto_repair
    auto_upgrade = var.k8s_auto_upgrade
  }

  node_config {
    preemptible  = true
    machine_type = var.k8s_machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      created_by = "terraform"
      env = var.workspace
    }

    tags = [
      "terraform",
      var.workspace
    ]
  }
}

# GCP doesn't provide the raw kubeconfig file so generate it ourselves
data "template_file" "kubeconfig" {
  count = var.k8s_enabled ? 1 : 0
  template = file("${path.module}/templates/kubeconfig.yaml")

  vars = {
    cluster_name = google_container_cluster.primary[count.index].name
    endpoint = google_container_cluster.primary[count.index].endpoint
    user_name = google_container_cluster.primary[count.index].master_auth.0.username
    user_password = google_container_cluster.primary[count.index].master_auth.0.password
    cluster_ca = google_container_cluster.primary[count.index].master_auth.0.cluster_ca_certificate
    client_cert = google_container_cluster.primary[count.index].master_auth.0.client_certificate
    client_cert_key = google_container_cluster.primary[count.index].master_auth.0.client_key
  }
}

