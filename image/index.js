/**
 * index
 *
 * This is a very simple application to simulate an
 * application with a database connection. It should
 * not be considered production-ready in any way, shape
 * or form.
 */

/* Node modules */
const { promises: fs } = require('fs');
const http = require('http');

/* Third-party modules */
const mysql = require('mysql2/promise');

/* Files */

const port = Number(process.env.PORT || 3000);

const server = http.createServer(async (req, res) => {
  console.log(`New request: ${req.method}:${req.url}`);

  const connection = await mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
  });

  await connection.query('INSERT INTO table_name SET ?', {
    text: `${req.method}:${req.url}`,
    createdAt: new Date(),
  });

  const [rows] = await connection.query('SELECT COUNT(*) AS count FROM table_name');

  const output = {
    rows,
    version: (await fs.readFile('./VERSION', 'utf8'))
      .replace('\n', ''),
    sealedSecretValue: process.env.SEALED_SECRET_VALUE || '--unset--',
  };

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(output));
});

server.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
