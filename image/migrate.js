/**
 * migrate
 *
 * Creates the MySQL database in a known state
 */

/* Node modules */

/* Third-party modules */
const mysql = require('mysql2/promise');

/* Files */

async function main() {
  const connection = await mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
  });

  await connection.execute(`CREATE TABLE IF NOT EXISTS table_name(
    id int auto_increment,
    text varchar(255) null,
    createdAt timestamp null,
    constraint table_pk primary key (id)
  )`, []);
}

main()
  .then(() => {
    console.log('Finished');
    process.exit();
  })
  .catch((err) => {
    console.log(err.stack);
    process.exit(1);
  });
